# Copyright 2013-2017 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

# Get these from src/stage0.txt
myexparam date=
exparam -v RUST_DATE date
myexparam rustc_required=
exparam -v RUSTC_REQUIRED rustc_required
myexparam bootstrap_rustc=${RUSTC_REQUIRED}
exparam -v BOOTSTRAP_RUSTC bootstrap_rustc
myexparam bootstrap_cargo=${BOOTSTRAP_RUSTC}
exparam -v BOOTSTRAP_CARGO bootstrap_cargo
myexparam importance=
exparam -v IMPORTANCE importance
myexparam -b dev=false

require cargo alternatives toolchain-funcs

export_exlib_phases pkg_preinst src_fetch_extra src_unpack src_prepare src_configure src_compile src_install

RUST_TARGET="$(exhost --target)"
RUST_TARGET="${RUST_TARGET/pc/unknown}"

if ever is_scm; then
    MY_PNV="${PNV}"
else
    MY_PNV="${PN}c-${PV}-src"
fi

if [[ "${RUST_TARGET}" == "x86_64-unknown-linux-musl" ]] && [[ "${RUST_DATE}" == "2018-03-29" ]]; then
    RUST_DATE="2018-04-30"
fi

RUSTC_SNAPSHOT="${PN}c-${BOOTSTRAP_RUSTC}-${RUST_TARGET}"
STD_SNAPSHOT="${PN}-std-${BOOTSTRAP_RUSTC}-${RUST_TARGET}"
CARGO_SNAPSHOT="cargo-${BOOTSTRAP_CARGO}-${RUST_TARGET}"

SUMMARY="A safe, concurrent, practical language"
DESCRIPTION="
Rust is a curly-brace, block-structured expression language. It visually resembles the C language
family, but differs significantly in syntactic and semantic details. Its design is oriented toward
concerns of “programming in the large”, that is, of creating and maintaining boundaries – both
abstract and operational – that preserve large-system integrity, availability and concurrency.
"

HOMEPAGE="https://www.rust-lang.org/"
LICENCES="MIT Apache-2.0"
MYOPTIONS="
    disable-sanitizers [[ description = [ disabled sanitizers (you shouldn't do that, but they don't build with glibc 2.26) ] ]]
    force-bootstrap [[ description = [ force boostrapping of rust in case autobootstrap doesn't work ] ]]
    internal-llvm [[ description = [ statically link to the internal llvm instead of the system one ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    ( libc: musl )
"

DEPENDENCIES="
    build:
        dev-lang/python:*
        sys-devel/cmake [[ description = [ for sanitizers (and llvm) ] ]]
        sys-devel/gcc:*[>=4.7]
        sys-devel/make[>=3.82]
        sys-devel/ninja [[ description = [ for sanitizers (and llvm) ] ]]
    build+run:
        net-libs/http-parser
        !internal-llvm? ( dev-lang/llvm:=[>=6.0] )
        libc:musl? ( dev-libs/libunwind [[ note = [ linked when statically linking for musl, see src/libunwind/lib.rs ] ]] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

WORK="${WORKBASE}/${MY_PNV}"

# Tests need gdb which does not work under sydbox
RESTRICT="test"

rust-build_get_bootstrap_slot() {
    local candidates=()
    local s

    if ever is_scm ; then
        # don't fail if repo has never been cloned or we are installing from pbin
        local _rev=$(nonfatal scm_call revision)
        if [[ -n ${_rev} ]] && [[ -x /usr/host/bin/rustc-${SLOT}-${_rev} ]]; then
            echo ${SLOT}
            return
        fi
    fi

    [[ ${SLOT} == beta ]]    && candidates+=(beta)
    [[ ${SLOT} == nightly ]] || candidates+=(stable)

    for s in "${candidates[@]}"; do
        if has_version "dev-lang/rust:${s}[>=${RUSTC_REQUIRED}]"; then
            echo ${s}
            return
        fi
    done
}

rust-build_has_build_deps() {
    local s=$(rust-build_get_bootstrap_slot)

    ! option force-bootstrap && [[ -n ${s} ]]
}

rust-build_src_fetch_extra() {
    if ever is_scm; then
        scm_src_fetch_extra
    else
        default
    fi

    if ! rust-build_has_build_deps; then
        local static_host="https://static.rust-lang.org"
        local old_path="${PATH}"
        local snap
        for path in ${PALUDIS_FETCHERS_DIRS[@]};do
            export PATH="${PATH}:${path}"
        done
        exparam -b dev && static_host="https://dev-static.rust-lang.org"

        # Note on how to create new snapshots:
        #  - generate the config.toml as it would be in this exlib with the following changes:
        #       build.openssl-static = true
        #       build.cargo, build.rustc shouldn't point to paludis' ${WORKBASE} ;)
        #  - run ./x.py dist
        #  - upload the toolchain binary distribution from build/dist
        libc-is-musl && static_host="https://dev.exherbo.org/distfiles/rust-bootstrap"

        for snap in ${RUSTC_SNAPSHOT} ${STD_SNAPSHOT} ${CARGO_SNAPSHOT}; do
            if ! [ -f "${FETCHEDDIR}/${snap}-${RUST_DATE}.tar.xz" ]; then
                dohttps "${static_host}/dist/${RUST_DATE}/${snap}.tar.xz" "${FETCHEDDIR}/${snap}-${RUST_DATE}.tar.xz"
            fi
        done
        export PATH="${old_path}"
    fi
}

rust-build_src_unpack() {
    local tool

    if ! rust-build_has_build_deps; then
        for snap in ${RUSTC_SNAPSHOT} ${STD_SNAPSHOT} ${CARGO_SNAPSHOT}; do
            unpack ${snap}-${RUST_DATE}.tar.xz
        done
        export CARGO="${WORKBASE}"/${CARGO_SNAPSHOT}/cargo/bin/cargo
        export RUSTC="${WORKBASE}"/${RUSTC_SNAPSHOT}/rustc/bin/rustc
    fi

    if ever is_scm; then
        scm_src_unpack
    else
        default
    fi

    edo cd "${WORK}"/src
    ecargo_fetch
}

rust-build_src_prepare() {
    # rust embeds several dependencies with hardcoded calls to unprefixed tools
    # This is mostly copied from chromium.exlib
    local dir="${WORKBASE}"/symlinked-build-tools
    local tool
    edo mkdir -p "${dir}"
    # ar is needed by compiler-rt
    # cc is needed only when bootstraping
    # gcc is needed or else we hit a weird linkage problem
    for tool in ar cc gcc; do
        edo ln -s /usr/host/bin/$(exhost --tool-prefix)${tool} "${dir}"/${tool}
    done

    export PATH="${dir}:${PATH}"

    if ! rust-build_has_build_deps; then
        edo mkdir -p build/${RUST_TARGET}
        edo ln -s "${WORKBASE}"/${RUSTC_SNAPSHOT}/rustc build/${RUST_TARGET}/stage0
        ever at_least 1.26 || edo mkdir -p build/${RUST_TARGET}/stage0/lib/rustlib/${RUST_TARGET}
        edo ln -s "${WORKBASE}"/${STD_SNAPSHOT}/rust-std-${RUST_TARGET}/lib/rustlib/${RUST_TARGET}/lib build/${RUST_TARGET}/stage0/lib/rustlib/${RUST_TARGET}/
        echo -n "${RUST_DATE}" > build/${RUST_TARGET}/stage0/.rustc-stamp
        echo -n "${BOOTSTRAP_CARGO}" > build/${RUST_TARGET}/stage0/.cargo-stamp
    fi

    # We fill config.toml with all the recent config keys, so don't fail o them on older versions
    edo sed -e 's/deny_unknown_fields,//' -i src/bootstrap/config.rs

    default
}

rust-build_src_configure() {
    local build=$(exhost --build)
    build=${build/pc/unknown}
    local bootstrap_slot=$(rust-build_get_bootstrap_slot)

    # Last checked commit: b183bd0ad40b0188fc242bfb9f68a0cf1704dbd9
    cat > config.toml << EOF
[llvm]
optimize = true
release-debuginfo = false
assertions = false
ccache = false
version-check = false
static-libstdcpp = false
ninja = true
# targets = let rust handle this
# experimental-targets = let rust handle this
link-jobs = 0
link-shared = false
[build]
build = "${build}"
host = ["${build}"]
target = ["${RUST_TARGET}"]
cargo = "${CARGO:-/usr/host/bin/cargo-${bootstrap_slot:-${SLOT}}}"
rustc = "${RUSTC:-/usr/host/bin/rustc-${bootstrap_slot:-${SLOT}}}"
docs = true
compiler-docs = false
submodules = false
fast-submodules = false
# gdb = only needed for tests, fail under sydbox
# nodejs = only needed for emscriptem
python = "python2.7"
locked-deps = true
vendor = false
full-bootstrap = false
extended = true
# tools = build all tools
verbose = 2
sanitizers = $(option disable-sanitizers false true)
openssl-static = false
low-priority = false
# configure-args = this has no use to us
# local-rebuild = let rust autodetect this
# print-step-timings = this has no use to us
[install]
prefix = "/usr/$(exhost --target)"
sysconfdir = "/etc"
docdir = "/usr/share/doc/${PNVR}"
bindir = "/usr/$(exhost --target)/bin"
libdir = "/usr/$(exhost --target)/lib"
mandir = "/usr/share/man"
datadir = "/usr/share"
infodir = "/usr/share/info"
localstatedir = "/var/lib"
[rust]
debug = false
optimize = true
codegen-units = 16
debug-assertions = false
debuginfo = false
debuginfo-lines = false
debuginfo-only-std = false
debuginfo-tools = false
use-jemalloc = $(libc-is-musl && echo false || echo true)
debug-jemalloc = false
backtrace = true
experimental-parallel-queries = false
default-linker = "$(exhost --tool-prefix)cc"
channel = "${SLOT}"
rpath = false
quiet-tests = false
optimize-tests = true
debuginfo-tests = false
codegen-tests = true
ignore-git = false
dist-src = false
test-miri = false
# save-toolstates = we don't need that
codegen-backends-dir = "codegen-${SLOT}-backends"
codegen-backends = ["llvm"]
# wasm-syscall = we don't need this
lld = false # file conflict between slots
deny-warnings = false
backtrace-on-ice = false
[target.${RUST_TARGET}]
cc = "$(exhost --tool-prefix)cc"
cxx = "$(exhost --tool-prefix)c++"
ar = "$(exhost --tool-prefix)ar"
linker = "$(exhost --tool-prefix)cc"
$(option !internal-llvm "llvm-config = \"/usr/host/bin/llvm-config\"")
# jemalloc = let rust handle this
# android-ndk = this has no use to us
crt-static = false
crt-included = false
$(libc-is-musl && echo "musl-root = \"/usr/$(exhost --target)\"")
# qemu-rootfs = this has no use to us
[dist]
src-tarball = false
EOF

    cargo_src_configure
}

rust-build_src_compile(){
    unset SUDO_USER
    esandbox allow "${CARGO_HOME}"
    edo ./x.py dist
    esandbox disallow "${CARGO_HOME}"
}

rust-build_src_install() {
    unset SUDO_USER
    esandbox allow "${CARGO_HOME}"
    DESTDIR="${IMAGE}" edo ./x.py install
    esandbox disallow "${CARGO_HOME}"

    local alternatives=()
    local rustlib_alt_files=(
        components
        install.log
        rust-installer-version
        uninstall.sh
        ${TARGET}/codegen-backends
    )
    local rustlibdir=/usr/$(exhost --target)/lib/rustlib
    local alt_dirs=(
        /etc/bash_completion.d
        /usr/share/zsh/site-functions
        /usr/$(exhost --target)/bin
        ${rustlibdir}/etc
        ${rustlibdir}/src
    )
    local dir
    local f

    edo pushd "${IMAGE}${rustlibdir}"
    for f in "${rustlib_alt_files[@]}" manifest-*; do
        alternatives+=( "${rustlibdir}/${f}" $(basename ${f} | sed -re 's/^([^-]*)(-?.*)$/\1-'${SLOT}'\2/') )
    done
    edo popd
    for dir in "${alt_dirs[@]}"; do
        edo pushd "${IMAGE}${dir}"
        for f in *; do
            alternatives+=( "${dir}/${f}" $(echo ${f} | sed -re 's/^([^-]*)(-?.*)$/\1-'${SLOT}'\2/') )
        done
        edo popd
    done
    edo pushd "${IMAGE}usr/share/man/man1"
    for f in *; do
        alternatives+=( "/usr/share/man/man1/${f}" $(echo ${f} | sed -re 's/^([^-]*)(-?.*).1$/\1-'${SLOT}'\2.1/') )
    done
    edo popd

    alternatives_for ${PN} ${SLOT} ${IMPORTANCE} "${alternatives[@]}"

    if ever is_scm; then
        local bin
        local bindir="/usr/$(exhost --target)/bin"
        local revision=$(scm_call revision)
        edo pushd "${IMAGE}${bindir}"
        for bin in *-${SLOT}; do
            dosym ${bin} ${bindir}/${bin}-${revision}
        done
        edo popd
    fi
}

rust-build_pkg_preinst() {
    if [[ -L "${ROOT}/etc/env.d/alternatives/${PN}/${SLOT}/usr/$(exhost --target)/lib/rustlib" ]]; then
        eclectic ${PN} unset
        edo rm "${ROOT}/etc/env.d/alternatives/${PN}/${SLOT}/usr/$(exhost --target)/lib/rustlib"
    fi
}
